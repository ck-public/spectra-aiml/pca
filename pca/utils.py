# Standard:
import subprocess

# External
import numpy as np
import pandas as pd
from argparse import ArgumentParser

REPO_PATH = subprocess.check_output("git rev-parse --show-toplevel", shell=True).decode().replace("\n", "")
DATA_PATH = f"{REPO_PATH}/../data"

# Load a CSV File into a Pandas DataFrame:
def load_data(filename):
    dataset = pd.read_csv(f"{DATA_PATH}/{filename}")
    return dataset


# Format the Dataset to the Following: 
# x = [a11, a12, ..., a1n
#      a21, a22, ..., a2n
#      ...
#      an1, an2, ..., ann]
# y = [0.05, 0.05125, ..., 3.0]
def format_dataset(dataset):
    features = []
    columns = list(dataset.columns)[1:]
    wavenumbers = dataset.iloc[:, 0].to_numpy()
    for column in columns:
        features.append(dataset[[column]].to_numpy())

    labels = np.array(columns)
    features = np.array(features).squeeze()
    return wavenumbers, features, labels


# Print Characteristics of Trained PCA Object:
def print_pca(pca):
    print(f"---> Number of Components: {pca.n_components_}")
    print(f"---> Explained Variance: {pca.explained_variance_}")
    print(f"---> Explained Variance Ratio: {pca.explained_variance_ratio_}")
    print(f"---> Singular Values: {pca.singular_values_}")
    print(f"---> Number of Features: {pca.n_features_}")
    print(f"---> Number of Samples: {pca.n_samples_}")
    print(f"---> Noise Variance: {pca.noise_variance_}")
    print(f"---> Singular Values Shape: {pca.singular_values_.shape}")
    print(f"---> Singular Values: {pca.singular_values_}")
    print(f"---> Loading Shape: {pca.components_.shape}\n")


# Print Characteristics of Trained StandardScaler Object:
def print_scaler(scaler):
    print(f"---> Scaler Means: {scaler.mean_}")
    print(f"---> Number of Means: {len(scaler.mean_)}")
    print(f"---> Scaler Vars: {scaler.var_}")
    print(f"---> Number of Vars: {len(scaler.var_)}")
    print(f"---> Number of Features: {scaler.n_features_in_}")


if __name__ == "__main__":
    parser = ArgumentParser("Run Python Utilities on a Diven Dataset")
    parser.add_argument("--path", type=str, default="electrolyte_data.csv", help="Path to Dataset")
    parser.add_argument("--dataset", action="store_true", help="Load Dataset and Plot Spectra")
    args = parser.parse_args()
    print(f"\nArguments: {args}\n")

    if args.dataset:
        dataset = load_data(args.path)
        print(dataset.describe())
