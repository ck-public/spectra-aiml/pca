import pandas as pd
import numpy as np
import random as rd
import matplotlib.pyplot as plt

#loading data
# path = "results/lithium/03-29-2022T11:09:06/pca_data.csv"
path = "results/lithium/11-02-2022T10:19:13/pca_data.csv"
X = pd.read_csv(path)


# Return Random Samples from Dataset:
def get_random_centroids(X, k=3):
    return (X.sample(n = k))


def k_means_fit(X, centroids, n=5):
    # Create a Copy of the Original Data:
    X_data = X
    diff = 1
    j = 0

    while(diff != 0):
        i = 1

        # Iterate Over Each Centroid:
        for _, row_c in centroids.iterrows():
            ED = []

            # Iterate Over Each Data Point:
            for _, row_d in X_data.iterrows():
                # Calculate Euclidean Distance Between Current Point and Centroid:
                d = np.sqrt((row_c["1"] - row_d["1"])**2 + (row_c["2"] - row_d["2"])**2)
                ED.append(d)

            # Append Data Distances for a Centroid to Original Data Frame:
            X[i] = ED
            i = i + 1

        C = []
        for _, row in X.iterrows():
            # Get Distance from Centroid of Current Data Point:
            min_dist = row[1]
            pos = 1

            # Loop to Locate the Closest Centroid to Current Point:
            for i in range(n):
                if row[i + 1] < min_dist:
                    min_dist = row[i + 1]
                    pos = i + 1
            C.append(pos)

        # Assigning Closest Cluster to Each Data Point:
        X["Cluster"] = C

        # Grouping Each Cluster by Mean Value to Create New Centroids:
        centroids_new = X.groupby(["Cluster"]).mean()[["2", "1"]]
        if j == 0:
            diff = 1
            j = j + 1
        else:
            # check if there is a difference between old and new centroids
            diff = (centroids_new["2"] - centroids["2"]).sum() + (centroids_new["1"] - centroids["1"]).sum()
            print(diff.sum())

        centroids = X.groupby(["Cluster"]).mean()[["2", "1"]]
        
    return X, centroids

centroids = get_random_centroids(X, k = 4)
clustered, cent = k_means_fit(X, centroids, n= 4)

#setting color values for our 
color = ["brown", "blue", "green", "cyan"]

#plot data
for k in range(len(color)):
    cluster = clustered[clustered["Cluster"] == (k + 1)]
    plt.scatter(cluster["1"], cluster["2"], c=color[k])
    
#plot centroids    
plt.scatter(cent["1"], cent["2"], c="red")
plt.xlabel("PC1")
plt.ylabel("PC2")
plt.savefig("round1.png")

def get_kmeans_pp_centroids(X1, k = 5):
    centroids = X1.sample()
    print(centroids)
    i = 1
    dist = []
    while i != k:
        max_dist = [0, 0]
        #go through the centroids
        for index, row in centroids.iterrows():
            #calculate distance of every centroid with every other data point 
            d = np.sqrt((X1["1"] - row["1"])**2 + (X1["2"] - row["2"])**2)
            #check which centroid has a max distance with another point
            if max(d) > max(max_dist):
                max_dist = d

        X1 = pd.concat([X1, max_dist], axis = 1)
        idx = X1.iloc[:, i + 1].idxmax()
        max_coor = pd.DataFrame(X1.iloc[idx][["1", "2"]]).T
        centroids = pd.concat([centroids, max_coor])
        X1 = X1.drop(idx)
        i += 1
    return centroids

centroids = get_kmeans_pp_centroids(X, k=4)
clustered, cent = k_means_fit(X, centroids, n= 4)

#setting color values for our 
color=["brown", "blue", "green", "cyan"]

#plot data
for k in range(len(color)):
    cluster=clustered[clustered["Cluster"]==k+1]
    plt.scatter(cluster["1"], cluster["2"], c=color[k])
    
#plot centroids    
plt.scatter(cent["1"], cent["2"], c="red")
plt.xlabel("PC1")
plt.ylabel("PC2")
plt.savefig("round2.png")