import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from argparse import ArgumentParser
from kneed import DataGenerator, KneeLocator
from sklearn.preprocessing import MinMaxScaler

def load_data(path):
    dataset = pd.read_csv(path)
    return dataset


def locate_knee(dataset):
    data = dataset[["1", "2"]]
    print(data.describe())
    scaler = MinMaxScaler()
    data = scaler.fit_transform(data)
    data = pd.DataFrame(data, columns=["1", "2"])
    print(data.describe())
    x = data["1"].values
    y = data["2"].values
    print(f"X Shape: {x.shape}")
    print(f"Y Shape: {y.shape}")

    kneedle = KneeLocator(x, y, S=1.0)
    print(f"Knee Location X: {kneedle.knee}")
    print(f"Knee Location Y: {kneedle.knee_y}")

    # kneedle.plot_knee_normalized()
    kneedle.plot_knee()
    plt.show()


if __name__ == "__main__":
    # parser = ArgumentParser("Cluster a Specific Concentration Spectra")
    # parser.add_argument("--path", type=str, default="electrolyte_data.csv")
    # parser.add_argument("--molar", type=str, choices=["0.5M", "0.75M", "1M", "1.25M", "1.5M"], default="0.5M")
    # args = parser.parse_args()
    # print(f"\nArguments: {args}\n")
    path = "results/lithium/03-29-2022T11:09:06/pca_data.csv"
    dataset = load_data(path)
    print(dataset.describe())
    locate_knee(dataset)
