# Standard:
import random
from itertools import combinations

# External:
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pathlib import Path

# Local:
from utils import DATA_PATH

# Plotting Globals:
DPI = 80
FIGURE_SIZE = (40, 30)
TITLE_PLOT_KWARGS = {
    "fontsize": 90,
    "fontweight": "bold"
}
LABEL_PLOT_KWARGS = {
    "fontsize": 80,
    "fontweight": "bold"
}
LEGEND_PLOT_KWARGS = {
    "fontsize": 80
}
MARKER_SIZE = 1000
SMALLER_MARKER_SIZE = 800
FIGURES = {
    2: {"size": (40, 60), "subplots": (2, 1)},
    3: {"size": (40, 90), "subplots": (3, 1)},
    4: {"size": (80, 60), "subplots": (2, 2)}
}


def plot_spectra(concentrations, spectra, scaled_spectra, wavenumbers, plots_path):
    # Plot All Original Spectra Together:
    plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
    for i, s in enumerate(spectra):
        plt.plot(wavenumbers, s, label=concentrations[i], linewidth=3)
    plt.axhline(y=0, color="k", linewidth=2)
    plt.title("All Spectra (Original)", **TITLE_PLOT_KWARGS)
    plt.xlabel("Wavenumber [cm^-1]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Absorption [-]", **LABEL_PLOT_KWARGS)
    plt.xticks(**LEGEND_PLOT_KWARGS)
    plt.yticks(**LEGEND_PLOT_KWARGS)
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.legend(**LEGEND_PLOT_KWARGS)
    plt.tight_layout()
    plt.savefig(plots_path / "spectra_all_original.png")
    plt.clf()

    # Plot Each Scaled Spectra Alone:
    for i, s in enumerate(scaled_spectra):
        plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
        plt.plot(wavenumbers, s, color="m", label=concentrations[i], linewidth=4)
        plt.axhline(y=0, color="k", linewidth=2)
        plt.title(f"{concentrations[i]} Spectra (Scaled)", **TITLE_PLOT_KWARGS)
        plt.xlabel("Wavenumber [cm^-1]", **LABEL_PLOT_KWARGS)
        plt.ylabel("Absorption [-]", **LABEL_PLOT_KWARGS)
        plt.xticks(**LEGEND_PLOT_KWARGS)
        plt.yticks(**LEGEND_PLOT_KWARGS)
        for spine in plt.gca().spines.values():
            spine.set_visible(False)

        plt.tight_layout()
        plt.savefig(plots_path / f"spectra_{concentrations[i]}_scaled.png")
        plt.clf()

    # Plot All Scaled Spectra Together:
    plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
    for i, s in enumerate(scaled_spectra):
        plt.plot(wavenumbers, s, label=concentrations[i], linewidth=3)
    plt.axhline(y=0, color="k", linewidth=2)
    plt.title("All Spectra (Scaled)", **TITLE_PLOT_KWARGS)
    plt.xlabel("Wavenumber [cm^-1]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Absorption [-]", **LABEL_PLOT_KWARGS)
    plt.xticks(**LEGEND_PLOT_KWARGS)
    plt.yticks(**LEGEND_PLOT_KWARGS)
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.legend(**LEGEND_PLOT_KWARGS)
    plt.tight_layout()
    plt.savefig(plots_path / "spectra_all_scaled.png")
    plt.clf()


# Plot Loadings from Trained PCA Object:
def plot_loadings(pca, scaler, spectra, scaled_spectra, wavenumbers, concentrations, plots_path):
    loadings_plots_path = Path(f"{plots_path}/loadings")
    loadings_plots_path.mkdir(parents=True, exist_ok=True)

    print("Plotting the Identified Loadings\n")
    loadings = pca.components_
    new_loadings = scaler.inverse_transform(loadings)
    for i, loading in enumerate(new_loadings):
        plt.figure(figsize=(80, 30), dpi=DPI)

        plt.subplot(1, 2, 1)
        plt.plot(wavenumbers, loadings[i], linewidth=4)
        plt.title(f"PCA Loading {i} Standardized", **TITLE_PLOT_KWARGS)
        plt.xlabel("Wavenumber", **LABEL_PLOT_KWARGS)
        plt.ylabel("Loading (Spectra)", **LABEL_PLOT_KWARGS)

        plt.subplot(1, 2, 2)
        plt.plot(wavenumbers, loading, linewidth=4)
        plt.title(f"PCA Loading {i} Original", **TITLE_PLOT_KWARGS)
        plt.xlabel("Wavenumber", **LABEL_PLOT_KWARGS)
        plt.ylabel("Loading (Spectra)", **LABEL_PLOT_KWARGS)

        plt.tight_layout()
        plt.savefig(loadings_plots_path / f"pca_loading_{i}.png")
        plt.clf()

    # Plot Loadings vs Spectra:
    plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
    plt.title(f"Original Spectra vs Loadings", **TITLE_PLOT_KWARGS)
    plt.xlabel("Wavenumber", **LABEL_PLOT_KWARGS)
    plt.ylabel("Absorbtance", **LABEL_PLOT_KWARGS)
    plt.axhline(y=0, color='k')
    for i, loading in enumerate(new_loadings):
        plt.plot(wavenumbers, loading / 10, label=f"loading {i + 1}", linewidth=4)

    for i, spectrum in enumerate(spectra):
        plt.plot(wavenumbers, spectrum, label=concentrations[i], linewidth=2)

    for spine in plt.gca().spines.values():
        spine.set_visible(False)
    plt.legend(**LEGEND_PLOT_KWARGS)
    plt.xticks(**LEGEND_PLOT_KWARGS)
    plt.yticks(**LEGEND_PLOT_KWARGS)
    plt.savefig(loadings_plots_path / f"spectra_vs_loadings.png")
    plt.clf()

    # Loadings Combinations:
    combos = [list(item) for item in combinations(range(len(new_loadings)), 2)]
    for combo in combos:
        loading_id_1 = combo[0] + 1
        loading_id_2 = combo[1] + 1
        plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
        plt.xlabel("Wavenumber", **LABEL_PLOT_KWARGS)
        plt.ylabel("Loading (Spectra)", **LABEL_PLOT_KWARGS)
        plt.title(f"Loadings {loading_id_1} and {loading_id_2}", **TITLE_PLOT_KWARGS)
        plt.plot(wavenumbers, new_loadings[combo[0]], label=f"loading {loading_id_1}", linewidth=4)
        plt.plot(wavenumbers, new_loadings[combo[1]], label=f"loading {loading_id_2}", linewidth=4)
        plt.axhline(y=0, color='k', linewidth=2)
        plt.xticks(**LEGEND_PLOT_KWARGS)
        plt.yticks(**LEGEND_PLOT_KWARGS)
        for spine in plt.gca().spines.values():
            spine.set_visible(False)
        plt.legend(fontsize=int(LABEL_PLOT_KWARGS["fontsize"]), loc="upper right")
        plt.savefig(loadings_plots_path / f"loadings_{loading_id_1}_and_{loading_id_2}.png")
        plt.clf()

        # Plot the Difference in Loadings vs Loadings:
        difference = new_loadings[combo[0]] - new_loadings[combo[1]]
        plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
        plt.xlabel("Wavenumber", **LABEL_PLOT_KWARGS)
        plt.ylabel("Loading (Spectra)", **LABEL_PLOT_KWARGS)
        plt.title(f"Loadings {loading_id_1} and {loading_id_2} vs Difference", **TITLE_PLOT_KWARGS)
        plt.plot(wavenumbers, new_loadings[combo[0]], label=f"loading {loading_id_1}", linewidth=4)
        plt.plot(wavenumbers, new_loadings[combo[1]], label=f"loading {loading_id_2}", linewidth=4)
        plt.plot(wavenumbers, difference * 100, label="difference * 100", linewidth=4)
        plt.axhline(y=0, color='k', linewidth=2)
        plt.xticks(**LEGEND_PLOT_KWARGS)
        plt.yticks(**LEGEND_PLOT_KWARGS)
        for spine in plt.gca().spines.values():
            spine.set_visible(False)
        plt.legend(fontsize=int(LABEL_PLOT_KWARGS["fontsize"]), loc="upper right")
        plt.savefig(loadings_plots_path / f"difference_in_loadings_vs_loadings_{loading_id_1}_and_{loading_id_2}.png")
        plt.clf()

        # Plot Difference in Loadings vs Spectra:
        plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
        plt.title(f"Spectra vs Loadings Difference ({loading_id_1} and {loading_id_2})", **TITLE_PLOT_KWARGS)
        plt.xlabel("Wavenumber", **LABEL_PLOT_KWARGS)
        plt.ylabel("Absorbtance", **LABEL_PLOT_KWARGS)
        plt.axhline(y=0, color='k', linewidth=2)
        plt.plot(wavenumbers, difference * 100, label="difference * 100", linewidth=4)
        for i, spectrum in enumerate(spectra):
            plt.plot(wavenumbers, spectrum, label=concentrations[i], linewidth=2)

        for spine in plt.gca().spines.values():
            spine.set_visible(False)
        plt.legend(**LEGEND_PLOT_KWARGS, loc="upper right")
        plt.xticks(**LEGEND_PLOT_KWARGS)
        plt.yticks(**LEGEND_PLOT_KWARGS)
        plt.savefig(loadings_plots_path / f"spectra_vs_loadings_difference_{loading_id_1}_and_{loading_id_2}.png")
        plt.clf()


# Plot the Identified Compoents for the Dataset:
def plot_components(results, plots_path):
    components_plots_path = Path(f"{plots_path}/components")
    components_plots_path.mkdir(parents=True, exist_ok=True)

    print("Plotting the Identified Components\n")
    columns = [item for item in list(results.columns) if not item == "Molarity"]
    for column in columns:
        plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
        plt.xlabel(f"Molarity", **LABEL_PLOT_KWARGS)
        plt.ylabel(f"Principal Component {column}", **LABEL_PLOT_KWARGS)
        plt.title(f"PCA Component {column} vs Molarity", **TITLE_PLOT_KWARGS)
        for row in results.iterrows():
            molar = float(str(row[1]["Molarity"]).replace("M", ""))
            plt.scatter(molar, row[1][column], s=MARKER_SIZE)
        plt.grid()
        plt.savefig(components_plots_path / f"pca_component_{column}.png")
        plt.clf()

    # Iterate Over Component Combinations and Plot:
    print("Plotting the Identified Component Combinations\n")
    component_combinations = [list(c) for c in combinations(columns, 2)]
    print(f"\n\nCOMBOS: {component_combinations}\n\n")
    for combo in component_combinations:
        component_id_1 = int(combo[0])
        component_id_2 = int(combo[1])
        plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
        plt.xlabel(f"Principal Component {component_id_1}", **LABEL_PLOT_KWARGS)
        plt.ylabel(f"Principal Component {component_id_2}", **LABEL_PLOT_KWARGS)
        plt.title(f"PCA Component {component_id_2} vs Component {component_id_1}", **TITLE_PLOT_KWARGS)
        for row in results.iterrows():
            plt.scatter(row[1][combo[0]], row[1][combo[1]], s=MARKER_SIZE)

        plt.xticks(**LEGEND_PLOT_KWARGS)
        plt.yticks(**LEGEND_PLOT_KWARGS)
        for spine in plt.gca().spines.values():
            spine.set_visible(False)
        plt.savefig(components_plots_path / f"pca_component_comparison_{component_id_1}_{component_id_2}.png")
        plt.clf()


# Recalculate and Plot Spectra vs Original Spectra:
def plot_variance(pca, scaler, spectra, scores, wavenumbers, concentrations, plots_path):
    # Plotting the Variance with the Spectra:
    print(f"Plotting the Variance of Principal Component Combinations\n")
    plt.figure(figsize=(80, 30), dpi=DPI)
    colors = ["g", "m", "k", "b"]
    variances = []
    for i in range(1, pca.n_components_ + 1):
        # Recalculate Spectra from Trained PCA Model:
        centered_spectra = np.dot(scores[:, 0:i], pca.components_[0:i])
        derived = scaler.inverse_transform(centered_spectra)

        diffs = derived - spectra
        power = np.power(diffs, 2)
        wn_sum = np.sum(power, axis=0)
        variance = (wn_sum / (spectra.shape[0] - 1)) * 1000
        plt.plot(wavenumbers, variance, label=f"{i}_components", color=colors[i - 1], linewidth=2)
        plt.fill(wavenumbers, variance, facecolor=colors[i - 1], alpha=1)
        variances.append(variance)

    print(f"---> Variance Shape: {variances[0].shape}\n")
    for i, spectrum in enumerate(spectra):
        plt.plot(wavenumbers, spectrum, label=concentrations[i], linewidth=2)
    
    plt.xlabel(f"Wavenumber [-]", **LABEL_PLOT_KWARGS)
    plt.ylabel(f"Absorbance / Variance * 1000", **LABEL_PLOT_KWARGS)
    plt.title(f"Variance vs Wavenumber", **TITLE_PLOT_KWARGS)
    plt.xticks(**LEGEND_PLOT_KWARGS)
    plt.yticks(**LEGEND_PLOT_KWARGS)
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.legend(**LEGEND_PLOT_KWARGS)
    plt.tight_layout()
    plt.savefig(plots_path / f"variance_vs_wavenumber_with_spectra.png")
    plt.clf()

    # Plotting the Variance Alone:
    plt.figure(figsize=(160, 60), dpi=DPI)
    for i, variance in enumerate(variances, start=1):
        plt.plot(wavenumbers, variance, label=f"{i}_components", linewidth=2)
    
    plt.xlabel(f"Wavenumber [-]", **LABEL_PLOT_KWARGS)
    plt.ylabel(f"Variance * 1000 [-]", **LABEL_PLOT_KWARGS)
    plt.title(f"Variance vs Wavenumber", **TITLE_PLOT_KWARGS)
    plt.xticks(**LEGEND_PLOT_KWARGS)
    plt.yticks(**LEGEND_PLOT_KWARGS)
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.legend(**LEGEND_PLOT_KWARGS)
    plt.tight_layout()
    plt.savefig(plots_path / f"variance_vs_wavenumber.png")
    plt.clf()

    # Split Variance per Components into Subplots:
    figure_details = FIGURES[pca.n_components_]
    plt.figure(figsize=figure_details["size"], dpi=DPI)
    for i, variance in enumerate(variances, start=1):
        plt.subplot(*figure_details["subplots"], i)
        plt.plot(wavenumbers, variance, label=f"{i}_components", linewidth=2)

        plt.xlabel(f"Wavenumber [-]", **LABEL_PLOT_KWARGS)
        plt.ylabel(f"Variance * 1000 [-]", **LABEL_PLOT_KWARGS)
        plt.title(f"Variance vs Wavenumber {i} Components", **TITLE_PLOT_KWARGS)
        plt.xticks(**LEGEND_PLOT_KWARGS)
        plt.yticks(**LEGEND_PLOT_KWARGS)
        for spine in plt.gca().spines.values():
            spine.set_visible(False)

    plt.tight_layout()
    plt.savefig(plots_path / f"variance_vs_wavenumber_subplots.png")
    plt.clf()


# Recalculate and Plot Spectra vs Original Spectra:
def plot_calculated_spectra(pca, scaler, spectra, scores, wavenumbers, concentrations, plots_path):
    # Recalculate Spectra from Trained PCA Model:
    centered_spectra = np.dot(scores, pca.components_)
    derived = scaler.inverse_transform(centered_spectra)
    print(f"---> Original Spectra Shape: {spectra.shape}\n")

    # Select Indices to Plot
    if spectra.shape[0] == 5:
        indices = [1, 3]
    else:
        possibilities = list(range(spectra.shape[0]))
        indices = random.sample(possibilities, 2)

    plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
    for i, index in enumerate(indices):
        plt.subplot(2, 1, i + 1)
        difference = abs(derived[index] - spectra[index])
        plt.plot(wavenumbers, difference, color="m", linewidth=2, label="Difference")
        plt.title(f"Original Spectra Comparison {concentrations[index]}", **TITLE_PLOT_KWARGS)
        plt.xlabel("Wavenumber", **LABEL_PLOT_KWARGS)
        plt.ylabel("Absorbtance", **LABEL_PLOT_KWARGS)
        plt.legend(**LEGEND_PLOT_KWARGS)
        plt.xticks(**LEGEND_PLOT_KWARGS)
        plt.yticks(**LEGEND_PLOT_KWARGS)
        for spine in plt.gca().spines.values():
            spine.set_visible(False)
    
    plt.tight_layout()
    plt.savefig(plots_path / f"difference_in_derived_spectra_and_original_spectra.png")
    plt.close()
    plt.clf()

    plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
    for i, index in enumerate(indices):
        plt.subplot(2, 1, i + 1)
        plt.plot(wavenumbers, derived[index], color="b", linewidth=1, label="Derived")
        plt.plot(wavenumbers, spectra[index], color="m", linewidth=1, label="Original")
        plt.title(f"Original Spectra Comparison {concentrations[index]}", **TITLE_PLOT_KWARGS)
        plt.xlabel("Wavenumber", **LABEL_PLOT_KWARGS)
        plt.ylabel("Absorbtance", **LABEL_PLOT_KWARGS)
        plt.legend(**LEGEND_PLOT_KWARGS)
        plt.xticks(**LEGEND_PLOT_KWARGS)
        plt.yticks(**LEGEND_PLOT_KWARGS)
        for spine in plt.gca().spines.values():
            spine.set_visible(False)
    
    plt.tight_layout()
    plt.savefig(plots_path / f"derived_spectra_vs_original_spectra.png")
    plt.close()
    plt.clf()


# Plot Expected Changes in EC and EMC Concentration vs Original Spectra:
def plot_mole_fractions_vs_spectra(pca, scaler, spectra, wavenumbers, concentrations, plots_path):
    fractions_data = pd.read_csv(f"{DATA_PATH}/mole_fractions_msr_lithium.csv")
    ec_spectra = pd.read_csv(f"{DATA_PATH}/pure/ec_spectra.csv")
    emc_spectra = pd.read_csv(f"{DATA_PATH}/pure/emc_spectra.csv")

    plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
    plt.title(f"Mixed EC and EMC Spectra", fontsize=20, fontweight="bold")
    plt.xlabel("Wavenumber", fontsize=15, fontweight="bold")
    plt.ylabel("Absorbance", fontsize=15, fontweight="bold")
    mix_spectra = []
    for row in fractions_data.iterrows():
        mixed = row[1]["Mole Fraction EC"] * ec_spectra["Absorbance"] + row[1]["Mole Fraction EMC"] * emc_spectra["Absorbance"] 
        mix_spectra.append(mixed)
        concentration = row[1]["Concentration (mol/L)"]
        plt.plot(ec_spectra["Wavenumber"], mixed, label=f"{concentration}M")

    plt.legend(**LEGEND_PLOT_KWARGS)
    plt.xticks(**LEGEND_PLOT_KWARGS)
    plt.yticks(**LEGEND_PLOT_KWARGS)
    plt.savefig(plots_path / f"ec_emc_mixed_spectra.png")
    plt.clf()

    plt.title(f"Original Spectra vs EC and EMC Concentrations", fontsize=20, fontweight="bold")
    plt.xlabel("Wavenumber", fontsize=15, fontweight="bold")
    plt.ylabel("Absorbance", fontsize=15, fontweight="bold")
    plt.axhline(y=0, color='k')
    for i, spectrum in enumerate(spectra):
        plt.plot(wavenumbers, spectrum, label=concentrations[i])


    grayscale = [0.1, 0.3, 0.5, 0.7, 0.9]
    for i, mix in enumerate(mix_spectra):
        plt.plot(ec_spectra["Wavenumber"], mix, label=f"{concentrations[i]} Mix", color=str(grayscale[i]))

    ec_peaks = [1802.56, 1772.8, 1391.2, 1164.64, 1071.04, 772]
    emc_peaks = [1750.72, 1747.36, 1463.2, 1441.6, 1366.24, 1265.44, 1009.6, 934.72, 790.72]
    lipf6_peaks = [1486.19, 1111.33, 1008.02, 635.11, 554.62]
    for peak in ec_peaks:
        plt.axvline(x=peak, color='b', label="EC Peak")
    for peak in emc_peaks:
        plt.axvline(x=peak, color='r', label="EMC Peak")
    for peak in lipf6_peaks:
        plt.axvline(x=peak, color='k', label="LiPF6 Peak")

    plt.legend(**LEGEND_PLOT_KWARGS)
    plt.xticks(**LEGEND_PLOT_KWARGS)
    plt.yticks(**LEGEND_PLOT_KWARGS)
    plt.savefig(plots_path / f"mixed_vs_spectra.png")
    plt.clf()


# Plot All Possible Combinations of Identified Principal Components:
def plot_pca(results, scores, pca, scaler, spectra, scaled_spectra, wavenumbers, concentrations, plots_path):
    plot_spectra(concentrations, spectra, scaled_spectra, wavenumbers, plots_path)
    plot_components(results, plots_path)
    plot_loadings(pca, scaler, spectra, scaled_spectra, wavenumbers, concentrations, plots_path)
    plot_calculated_spectra(pca, scaler, spectra, scores, wavenumbers, concentrations, plots_path)
    plot_variance(pca, scaler, spectra, scores, wavenumbers, concentrations, plots_path)

    # Plot EC and EMC Peaks for Lithium Dataset:
    if not "TEGDME" in str(plots_path):
        plot_mole_fractions_vs_spectra(pca, scaler, spectra, wavenumbers, concentrations, plots_path)

    plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
    variance = pd.read_csv("explained_variance.csv")
    plt.xlabel(f"Number of Components [-]", **LABEL_PLOT_KWARGS)
    plt.ylabel(f"Variance [%]", **LABEL_PLOT_KWARGS)
    plt.title(f"Explained Variance vs Number of Components", **TITLE_PLOT_KWARGS)
    plt.scatter(variance["Components"], variance["Variance"], s=MARKER_SIZE)
    plt.xticks(**LEGEND_PLOT_KWARGS)
    plt.yticks(**LEGEND_PLOT_KWARGS)
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    # Calculate New Labels:
    locs, _ = plt.xticks()
    new_locs = []
    new_labels = []
    for loc in locs:
        if not ".5" in str(loc):
            new_locs.append(loc)
            new_labels.append(str(int(loc)))
    plt.xticks(new_locs, new_labels)

    plt.savefig(plots_path / f"explained_variance_vs_num_components.png")
    plt.clf()


# Plot the Battery Data Principal Components vs Training Data Principal Components:
def plot_battery_components(results, results_path, charges):
    plots_path = Path(f"{results_path}/battery")
    plots_path.mkdir(parents=True, exist_ok=True)

    training_dataset = pd.read_csv(results_path / "pca_data.csv")
    columns = [column for column in training_dataset.columns if not "Molarity" == column]
    combos = [list(c) for c in combinations(columns, 2)]
    colors = ["k", "b", "g"]

    for combo in combos:
        plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
        x = training_dataset[combo].to_numpy()
        int_combo = [int(c) - 1 for c in combo]
        plt.scatter(x[:, 0], x[:, 1], s=SMALLER_MARKER_SIZE, facecolors="m", edgecolors="m", linewidths=3, label="Training")
        for i, result in enumerate(results):
            plt.scatter(result[:, int_combo[0]], result[:, int_combo[1]], s=SMALLER_MARKER_SIZE, facecolors=colors[i], edgecolors=colors[i], linewidths=3, label=charges[i])
        plt.title(f"Battery Principle Components vs Training", **TITLE_PLOT_KWARGS)
        plt.xlabel(f"PC{int_combo[0] + 1}", **LABEL_PLOT_KWARGS)
        plt.ylabel(f"PC{int_combo[1] + 1}", **LABEL_PLOT_KWARGS)
        plt.xticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
        plt.yticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
        plt.legend(**LEGEND_PLOT_KWARGS)
        plt.savefig(plots_path / f"battery_vs_training_pc_comparison_{int_combo[0] + 1}_and_{int_combo[1] + 1}.png")

        for i, result in enumerate(results):
            plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
            plt.scatter(result[:, int_combo[0]], result[:, int_combo[1]], s=SMALLER_MARKER_SIZE, facecolors="m", edgecolors="m", linewidths=3)
            plt.title(f"Battery {charges[i]}C Principle Components", **TITLE_PLOT_KWARGS)
            plt.xlabel(f"PC{int_combo[0] + 1}", **LABEL_PLOT_KWARGS)
            plt.ylabel(f"PC{int_combo[1] + 1}", **LABEL_PLOT_KWARGS)
            plt.xticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
            plt.yticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
            plt.savefig(plots_path / f"battery_pc_comparison_{int_combo[0] + 1}_and_{int_combo[1] + 1}_{charges[i]}c.png")
