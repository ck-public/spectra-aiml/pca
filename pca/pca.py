# Standard:
import pickle
import datetime
import subprocess

# External:
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pathlib import Path
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from argparse import ArgumentParser

# Local:
from utils import load_data, format_dataset, print_pca, print_scaler, DATA_PATH
from plotting import plot_pca, plot_battery_components, plot_spectra

DPI = 80
FIGURE_SIZE = (40, 30)
TITLE_PLOT_KWARGS = {
    "fontsize": 90,
    "fontweight": "bold"
}
LABEL_PLOT_KWARGS = {
    "fontsize": 80,
    "fontweight": "bold"
}
LEGEND_PLOT_KWARGS = {
    "fontsize": 80
}
MARKER_SIZE = 1000
SMALLER_MARKER_SIZE = 800


# Run PCA on the Supplied Dataset:
def run_pca(dataset, components, plots_path):
    # Formatting the Dataset into Features (Spectra) and Labels (Concentration):
    print("Formatting Dataset\n")
    wavenumbers, features, labels = format_dataset(dataset)
    spectra = np.copy(features)
    print(f"---> Features Shape: {features.shape}")
    print(f"---> Labels Shape: {labels.shape}\n")

    # Standardize the Data:
    print("Standardizing the Dataset\n")
    print("---> Using StandardScaler\n")
    scaler = StandardScaler()
    features = scaler.fit_transform(features)
    print(f"---> Spectra Shape: {spectra.shape}")
    print(f"---> Scaled Spectra Shape: {features.shape}")
    print_scaler(scaler)

    # Run PCA on the Scaled Data:
    print("Running PCA\n")
    pca = PCA(n_components=components)
    pca.fit(features)
    pickle.dump(pca, open(plots_path / "pca.pkl", "wb"))
    print_pca(pca)

    # Transform Data Using Trained PCA Model:
    print("Running PCA Transform on Datasets\n")
    scores = pca.transform(features)  # Training Scores (Principal Components)
    print(f"---> Scores Shape: {scores.shape}")

    column_names = [str(i) for i in range(1, pca.n_components_ + 1)]
    results = pd.DataFrame(data=scores, columns=column_names)
    results["Molarity"] = [float(label.replace("M", "")) for label in labels]
    results.to_csv(plots_path / "pca_data.csv", index=False)

    # Plot the Results:
    print("Plotting the PCA Details\n")
    plot_pca(results, scores, pca, scaler, spectra, features, wavenumbers, labels, plots_path)


def regress_and_predict(training_dataset, scores, results_path):
    x = training_dataset[['1', '2']].to_numpy()
    y = training_dataset[['Molarity']].to_numpy()
    poly = PolynomialFeatures(degree=2)
    poly_variables = poly.fit_transform(x, y)
    poly_var_train, poly_var_test, res_train, res_test = train_test_split(poly_variables, y, test_size=0.3, random_state=4)
    regression = LinearRegression()  
    model = regression.fit(poly_var_train, res_train)
    score = model.score(poly_var_test, res_test)
    print(f"Linear Regression Score: {score}")

    poly_variables_new = poly.transform(scores)
    predictions = model.predict(poly_variables_new)
    print(f"Predictions: {predictions.shape}")
    print(f"Predictions: {predictions}") 

    times = pd.read_csv(f"{DATA_PATH}/battery/gitt_{rate}c_time.csv")
    plt.figure(figsize=(40, 30), dpi=DPI)
    plt.scatter(times["Time [min]"], predictions, s=SMALLER_MARKER_SIZE, facecolors="m", edgecolors="m", linewidths=3, label="Pred-NN")
    plt.title(f"PCA Quantitative", **TITLE_PLOT_KWARGS)
    plt.xlabel("Time [min]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Molarity [M]", **LABEL_PLOT_KWARGS)
    plt.xticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
    plt.yticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
    plt.savefig(results_path / f"pred_molarity_pca_{rate}c.png")
    plt.clf()


def predict(dataset, rate, plots_path):
    # Formatting the Dataset into Features (Spectra) and Labels (Concentration):
    print("Formatting Dataset\n")
    wavenumbers, spectra, labels = format_dataset(dataset)
    print(f"---> Spectra Shape: {spectra.shape}")
    print(f"---> Labels Shape: {labels.shape}\n")

    # Standardize the Data:
    print("Standardizing the Dataset\n")
    print("---> Using StandardScaler\n")
    scaler = StandardScaler()
    scaler.fit(spectra)
    spectra = scaler.transform(spectra)
    print(f"---> Features Shape: {spectra.shape}")

    # Run PCA on the Scaled Data:
    print("Loading the PCA Model\n")
    pca = pickle.load(open(plots_path / "pca.pkl", 'rb'))
    print_pca(pca)

    # Transform Data Using Trained PCA Model:
    print("Running PCA Transform on Datasets\n")
    if spectra.shape[-1] > 3105:
        spectra = spectra[:, :3105]
        print(f"------> Updated Test Data Shape: {spectra.shape}")
    scores = pca.transform(spectra)  # Training Scores (Principal Components)
    print(f"---> Scores Shape: {scores.shape}")

    # Load the PCA Transformed Training Set:
    training_dataset = pd.read_csv(plots_path / "pca_data.csv")
    print(f"---> Training Scores Shape: {training_dataset.shape}")
    # regress_and_predict(training_dataset, scores, plots_path)

    # Find Closest Molarity:
    # min_dists = spatial.distance.cdist(scores, training_scores)
    # locs = np.argmin(min_dists, axis=1)

    return scores


if __name__ == "__main__":
    parser = ArgumentParser("Run PCA on Given Spectra")
    parser.add_argument("-t", "--train", action="store_true", help="Train a New Model")
    parser.add_argument("-i", "--ion", type=str, default="lithium", choices=["lithium", "tegdme"], help="Electrolyte System to Model")
    parser.add_argument("-m", "--measure", type=str, default="msr", choices=["sim", "msr"], help="Use the Measured (msr) or Simulated (sim) Dataset")
    parser.add_argument("-c", "--components", type=int, default=2, help="Number of Components for PCA")

    parser.add_argument("-p", "--predict", action="store_true", help="Run Predictions on Trained Model")
    parser.add_argument("-s", "--shifted", action="store_true", help="Run Predictions on the Baseline Corrected Data")
    parser.add_argument("-b", "--battery", type=str, default="all", choices=["1.00", "2.33", "3.00", "all"], help="Battery Cycling Data for Model Predictions")
    args = parser.parse_args()
    print(f"\nArguments: {args}\n")

    if args.train:
        # Load the Dataset:
        print("Loading Dataset\n")
        path = f"electrolyte_data_{args.measure}_{args.ion}.csv"
        dataset = load_data(path)
        print(dataset.describe())

        # Create the Results Path:
        prefix = "lithium"
        if "tegdme" in path:
            prefix = "tegdme"
        timestamp = datetime.datetime.now().strftime("%m-%d-%YT%H:%M:%S")
        dataset_name = path.split(".")[0]
        plots_path = Path(f"results/{prefix}/{timestamp}")
        plots_path.mkdir(parents=True, exist_ok=True)

        # Run PCA:
        run_pca(dataset, args.components, plots_path)

    if args.predict:
        # Get the Latest Training Runs:
        runs = subprocess.getoutput(f"ls -t results/{args.ion}").split("\n")
        results_path = Path(f"results/{args.ion}/{runs[0]}")
        results = []

        charge_options = ["1.00", "2.33", "3.00"]
        if args.battery != "all":
            charge_options = [args.battery]
        
        for rate in charge_options:
            print(f"\nPredicting for Battery Data {rate}C\n")
            spectra_file = f"battery/gitt_{rate}c.csv"
            if args.shifted:
                spectra_file = f"battery/corrected/gitt_{rate}c.csv"

            dataset = load_data(spectra_file)

            results.append(predict(dataset, rate, results_path))

        plot_battery_components(results, results_path, charge_options)
