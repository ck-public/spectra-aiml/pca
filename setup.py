from setuptools import setup, find_packages

setup(
    name='spectra-pca',
    version='1.0',
    description='A repository containing PCA tools to analyze measured absorption spectra for various lithium ion salt concentration solutions.',
    author='CK',
    author_email='cobkind@gmail.com',
    package_dir={'.': '.'},
    packages=find_packages(),
    python_requires='>=3.6, <4',
    install_requires=[
        "keras==2.8.0",
        "kneed==0.7.0",
        "matplotlib==3.5.1",
        "numpy==1.22.2",
        "pandas==1.4.0",
        "protobuf==3.20.3",
        "scikit-learn==1.0.2",
        "scipy==1.8.0",
        "tensorflow-macos==2.8.0",
        "tensorflow-metal==0.3.0",
        "xlrd==2.0.1",
    ],
)